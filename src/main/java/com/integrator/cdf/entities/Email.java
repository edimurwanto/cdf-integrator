package com.integrator.cdf.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "email")
public class Email {

    public Email() {
    }

    @Id
    @GeneratedValue
    Integer emailId;
    String email;

    @ManyToMany(mappedBy = "emailList")
    Set<Order> orders;

    public Integer getEmailId() {
        return emailId;
    }

    public void setEmailId(Integer emailId) {
        this.emailId = emailId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
