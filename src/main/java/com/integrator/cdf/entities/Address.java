package com.integrator.cdf.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "address")
public class Address {

    public Address() {
    }

    @Id
    @GeneratedValue
    Integer addressId;
    String street;
    Integer number;
    Integer postalCode;
    String neighborhood;
    String city;
    String state;

    @ManyToMany(mappedBy = "addressList")
    Set<Order> orders;

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
