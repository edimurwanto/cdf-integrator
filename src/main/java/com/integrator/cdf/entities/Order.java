package com.integrator.cdf.entities;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Entity
@Table(name = "orders")
@TableGenerator(name="tab", initialValue=0, allocationSize=50)
public class Order {

    @Id
    Integer orderId;

    Integer orderSeq;
    String orderStatus;
    String firstName;
    String lastName;
    String documentType;
    Long document;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "emailList",
            joinColumns = @JoinColumn(name = "orderId"),
            inverseJoinColumns = @JoinColumn(name = "emailId"))
    Set<Email> emailList;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "phoneList",
            joinColumns = @JoinColumn(name = "orderId"),
            inverseJoinColumns = @JoinColumn(name = "phoneId"))
    Set<Phone> phoneList;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "addressList",
            joinColumns = @JoinColumn(name = "orderId"),
            inverseJoinColumns = @JoinColumn(name = "addressId"))
    Set<Address> addressList;

    Date creationDate;
    Date shippingEstimateDate;
    Integer totals;
    Integer itemsId;
    String itemsName;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderSeq() {
        return orderSeq;
    }

    public void setOrderSeq(Integer orderSeq) {
        this.orderSeq = orderSeq;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public Long getDocument() {
        return document;
    }

    public void setDocument(Long document) {
        this.document = document;
    }

    public Set<Email> getEmailList() {
        return emailList;
    }

    public void setEmailList(Set<Email> emailList) {
        this.emailList = emailList;
    }

    public Set<Phone> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(Set<Phone> phoneList) {
        this.phoneList = phoneList;
    }

    public Set<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(Set<Address> addressList) {
        this.addressList = addressList;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getShippingEstimateDate() {
        return shippingEstimateDate;
    }

    public void setShippingEstimateDate(Date shippingEstimateDate) {
        this.shippingEstimateDate = shippingEstimateDate;
    }

    public Integer getTotals() {
        return totals;
    }

    public void setTotals(Integer totals) {
        this.totals = totals;
    }

    public Integer getItemsId() {
        return itemsId;
    }

    public void setItemsId(Integer itemsId) {
        this.itemsId = itemsId;
    }

    public String getItemsName() {
        return itemsName;
    }

    public void setItemsName(String itemsName) {
        this.itemsName = itemsName;
    }

}
