package com.integrator.cdf.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "phone")
public class Phone {

    public Phone() {
    }

    @Id
    @GeneratedValue
    Integer phoneId;
    Integer areaCode;
    String phone;
    Integer NR_RAMAL;
    String receiverName;

    @ManyToMany(mappedBy = "phoneList")
    Set<Order> orders;

    public Integer getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(Integer phoneId) {
        this.phoneId = phoneId;
    }

    public Integer getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(Integer areaCode) {
        this.areaCode = areaCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getNR_RAMAL() {
        return NR_RAMAL;
    }

    public void setNR_RAMAL(Integer NR_RAMAL) {
        this.NR_RAMAL = NR_RAMAL;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }
}
