package com.integrator.cdf.repository;

import com.integrator.cdf.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Date;
import java.util.ArrayList;

public interface OrderRepository extends JpaRepository<Order, Integer> {

    Order findByOrderId(Integer orderId);
    Order findByOrderSeq(Integer orderSeq);
    ArrayList<Order> findByOrderStatus(String status);
    Order findByDocument(Long document);
    ArrayList<Order> findByEmailList_Email(String email);
    ArrayList<Order> findByPhoneList_Phone(String phone);
    Order findByCreationDate(Date creationDate);

}
