package com.integrator.cdf.controllers;

import com.integrator.cdf.entities.Order;
import com.integrator.cdf.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController()
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderRepository orderRepository;

    @GetMapping()
    public List<Order> findAll(){
        return orderRepository.findAll();
    }

    @GetMapping("/{id}")
    public Order findByOrderId(@PathVariable Integer id){
        return orderRepository.findByOrderId(id);
    }

    @GetMapping("/findByOrderSeq/{orderSeq}")
    Order findByOrderSeq(@PathVariable Integer orderSeq){
        return orderRepository.findByOrderSeq(orderSeq);
    }

    @GetMapping("/findByOrderStatus/{status}")
    List<Order> findByStatus( @PathVariable String status){
        return orderRepository.findByOrderStatus(status);
    }

    @GetMapping("/findByDocument/{document}")
    Order findByDocument(@PathVariable Long document){
        return orderRepository.findByDocument(document);
    }

    @GetMapping("/findByEmail/{email}")
    List<Order> findByEmailList(@PathVariable String email){
        return orderRepository.findByEmailList_Email(email);
    }

    @GetMapping("/findByPhone/{phone}")
    List<Order> findByPhoneList(@PathVariable String phone){
        return orderRepository.findByPhoneList_Phone(phone);
    }

    @GetMapping("/findByCreationDate/{creationDate}")
    Order findByCreationDate(@PathVariable Date creationDate){
        return orderRepository.findByCreationDate(creationDate);
    }

    @PostMapping()
    public Order create(@RequestBody Order order){
        return orderRepository.save(order);
    }

    @PutMapping()
    public Order update(@RequestBody Order order) throws Exception {
        if(order.getOrderId() == null) {
            throw new ResponseStatusException(HttpStatus.METHOD_NOT_ALLOWED, "Method not allowed");
        } else {
            Optional<Order> orderToUpdate = orderRepository.findById(order.getOrderId());
            if(!orderToUpdate.isPresent()) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Order not found!");
            return orderRepository.save(order);
        }

    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id){
        orderRepository.deleteById(id);
    }


}
